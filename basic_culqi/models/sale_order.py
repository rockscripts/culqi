# -*- coding: utf-8 -*-
from odoo import api, fields, models, _

class sale_order(models.Model):
    _inherit = 'sale.order'

    culqi_response = fields.Html( string="Culqi")
    culqi_json_response = fields.Text( string="JSON Culqi Response")