# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request
import werkzeug
from werkzeug import urls

from culqi.client import Culqi
from culqi.resources import Charge
from culqi.resources import Customer
from culqi.resources import Order
import mercadopago

from uuid import uuid4
import os, hashlib, decimal, datetime, re, json, math, sys, re, time


class culqi_controller(http.Controller):
    formsPath = str(os.path.dirname(os.path.abspath(__file__))).replace("controllers","")

    @http.route('/culqi/process_culqi_payment/',  methods=['POST'], type='json', auth="public", website=True)    
    def process_culqi_payment(self, **kw):

        _response = {}
        params = {}
        params['culqi_preference'] = kw.get('culqi_preference')
        params['token'] = kw.get('token')
        params['customer'] = kw.get('customer')
        params['enviroment'] = kw.get('enviroment')

        culqi = Culqi(str('pk_test_W0YRWk9ti6iKb0wW'), str('sk_test_35xdkzs16z40DGNA'))
        
        charge = Charge(client=culqi)
        customer = Customer(client=culqi)
        order = Order(client=culqi)
        
        order_number = str(params['culqi_preference']['title']).format(uuid4().hex[:4])

        customer_name = re.sub("[^a-zA-Z]+", "", str(params['customer']['name']))

        millis = int(round(time.time() * 1000)) + int(604800000)

        response2 = customer.create({
                                        "address": params['customer']['street'],
                                        "address_city": params['customer']['state_name'],
                                        "country_code": "PE",
                                        "email": params['customer']['email'],
                                        "first_name": str(customer_name)[:50],
                                        "last_name": "not defined",
                                        "phone_number": params['customer']['mobile'] if (len(params['customer']['mobile'])) else params['customer']['phone'],
                                    })
        response7 = None
        _customer = None
        if(int(response2['status']) == 400):
            response7 = customer.list()#customer.read(email="ventas@leolandeo.com")
            _customer = self.search_customer(response7, params['customer']['email'])
        else:
            _customer = response2['data']

        # validate with id stored in odoo to optimize algorithm
        culqi_order = order.create({
                                            "amount": params['culqi_preference']['amount'],
                                            "currency_code": "PEN",
                                            "description": params['culqi_preference']['description'],
                                            "order_number": str(order_number),
                                            "client_details": {
                                                    "first_name": customer_name,
                                                    "last_name": "not defined",
                                                    "email": params['customer']['email'],
                                                    "phone_number": params['customer']['mobile'] if (len(params['customer']['mobile'])) else params['customer']['phone'],
                                            },
                                            "expiration_date": 1893474000,
                                            "confirm": False,
                                    })

        if(int(culqi_order['status']) == 400):
            culqi_order_id = "chr_test_Z8x0T712Szjvw7QD"
        else:
            culqi_order_id = culqi_order['data']['id']

        # validate with id stored in odoo to optimize algorithm 
        response1 = charge.create({
                                        "title": str(order_number),
                                        "order_number": str(order_number),                                        
                                        "amount": params['culqi_preference']['amount'],
                                        "capture": True,
                                        "currency_code": "PEN",
                                        "description": params['culqi_preference']['description'],
                                        "email":  params['customer']['email'],
                                        "first_name": customer_name,
                                        "address": params['customer']['street'],
                                        "address_city": params['customer']['state_name'],
                                        "phone_number": params['customer']['mobile'] if (len(params['customer']['mobile'])) else params['customer']['phone'],
                                        "country_code": "PE",
                                        "installments": 0,
                                        "source_id": params['token'],
                                    })
        
        metadatada = {
                        "metadata": {
                                       "order_id": culqi_order_id
                                    }
                     }

        response5 = charge.update(id_=response1['data']['id'], data=metadatada)

        
        

        metadatada = {
            "metadata": {
                    "order_id": culqi_order_id
            }
        }
        response6 = customer.update(id_='cus_test_72rgjfLslgOgVdSB', data=metadatada)

        response3 = order.list()
        response4 = order.confirm(culqi_order_id)                             
        
        _response['response1'] = response1
        _response['response2'] = response2
        _response['response3'] = response3
        _response['response4'] = response4
        _response['response5'] = response5
        _response['response7'] = response7
        _response['_customer'] = _customer
        _response['_order'] = culqi_order
        _response['_status'] = response2['status']
        
        _response['token'] = kw.get('token')
        _response['culqi_preference'] = params['culqi_preference']

        return _response

        # status 400
        # ['data']['merchant_message']
    
    def search_customer(self, customers_response, email):
        customers = None
        customer = None
        if(customers_response['status'] == '200'):
            customers = customers_response['data']['items']
            for customer in customers:
                if(str(customer['email'])==str(email)):
                    return customer

        else:
            return customer

    @http.route('/culqi/get_culqi_acquirer/', methods=['POST'], type='json', auth="public", website=True)
    def get_mercadopago_acquirer(self, **kw):       
        response = {"acquirer":None,"form_bill":None}                
        query = "select name, website_id,company_id, environment, website_published, provider, culqi_public_key from payment_acquirer where provider = 'culqi' limit 1"
        request.cr.execute(query)    
        acquirer = request.cr.dictfetchone()        
        response = {"acquirer":acquirer}
        return response
    
        
    @http.route('/culqi/get_sale_order/', methods=['POST'], type='json', auth="public", website=True)
    def get_sale_order(self, **kw): 
        params = {}
        params['acquirer_id'] = kw.get('acquirer_id')
        params['partner_id'] = kw.get('partner_id')
                       
        query = "select name, website_id,company_id, environment, website_published, provider, culqi_public_key from payment_acquirer where provider = 'culqi' limit 1"
        request.cr.execute(query)
        acquirer = request.cr.dictfetchone()
         
        if(acquirer['environment']=="test"):
            environment = str('1')
        else:
            environment = str('0')

        query = "select id, name, amount_total, amount_tax, date_order, partner_shipping_id from sale_order where partner_id = '"+str(params['partner_id'])+"' and state = '"+str('draft')+"' order by date_order desc limit 1"
        request.cr.execute(query)    
        draft_order = request.cr.dictfetchone() 

        product_titles = str("")
        separator = str("\n")
                  
       
        query = "select res_partner.id, res_partner.name, res_partner.vat, res_partner.phone, res_partner.mobile, res_partner.email, res_partner.street, res_partner.city, res_partner.zip, res_partner.lang, res_country.name as country_name, res_country.code as country_code, res_country_state.name as state_name, res_currency.name as currency_name, res_currency.symbol as currency_symbol from res_partner left join res_country on res_country.id = res_partner.country_id left join res_country_state on res_country_state.id = res_partner.state_id left join res_currency on res_country.currency_id = res_currency.id   where res_partner.id = '"+str(draft_order['partner_shipping_id'])+"' limit 1"
        request.cr.execute(query)    
        res_partner_shipping = request.cr.dictfetchone()
  
        if(draft_order):               
            order_name = str(datetime.datetime.now())
            order_name = re.sub('[^0-9]','', order_name)
            order_name = order_name[-9:]
            millis = int(round(time.time() * 1000)) + int(604800000)
            order_name['name'] = order_name['name'] + str('-') + str(millis)
            
            # base url
            query = "select value from ir_config_parameter where key = 'web.base.url' limit 1"
            request.cr.execute(query)
            ir_config_parameter = request.cr.dictfetchone()
            base_url = ir_config_parameter['value']

            draft_order_lines = http.request.env['sale.order.line'].sudo().search([['order_id','=',draft_order["id"]]])   
            checkout_items = []
            checkout_taxes = []
            for order_line in draft_order_lines:
                product_titles = str(product_titles) + str(order_line.name) + separator
                checkout_item = {
                                    "title":order_line.name,
                                    "quantity":order_line.product_uom_qty,
                                    "currency_id":order_line.currency_id.name,
                                    "unit_price":int(math.ceil(order_line.price_unit))
                                }
                checkout_items.append(checkout_item)
                #product_taxes = http.request.env['product.taxes.rel'].sudo().search([['prod_id','=',order_line.product_id.id]])
                query = "select tax_id from product_taxes_rel where prod_id = " + str(order_line.product_id.id)
                request.cr.execute(query)
                product_taxes = request.cr.dictfetchall()

                for product_tax in product_taxes:
                    taxes_details = http.request.env['account.tax'].sudo().search([['id','=',product_tax['tax_id']]])  
                     
                    if(int(taxes_details.amount)>0):
                        tax = {"type":"iva","value":int(taxes_details.amount)}
                        if(checkout_taxes.__len__()>0):
                            for tax_added in checkout_taxes:                            
                                if(int(tax_added["value"])!=int(taxes_details.amount)):
                                    checkout_taxes.append(tax)
                        else:
                            checkout_taxes.append(tax)                                

            jsonPreference = str("")
            return {
                        'status' :  "OK",
                        'environment':environment,                        
                        'json_preference':draft_order,
                        'product_lines':str(product_titles)[:75],
                        'customer':res_partner_shipping,
                    }

           